﻿import React, { useState } from "react";

export default function ButtonClicker() {
    const [count, setCount] = useState(0);
    return <button onClick={() => setCount(count + 1)}>Clicks: {count}</button>
}
