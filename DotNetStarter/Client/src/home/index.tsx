import "./index.css"

import React from "react";
import ReactDOM from "react-dom";
import ButtonClicker from "../components/ButtonClicker";

function Home() {
    return <div className="home">
        <ButtonClicker />
    </div>
}

export function init() {
    ReactDOM.render(<Home />, document.getElementById("root"));
}
